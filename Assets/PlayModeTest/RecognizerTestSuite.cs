using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Algorithms;

namespace TestRecognizer
{
    
    /// <summary>
    /// Testsuite which tests all the main function uses to recognize a gesture
    /// </summary>
    public class RecognizerTestSuite
    {

        private GameObject gameManager;

        /// <summary>
        /// points is the list of point of a line
        /// </summary>
        /// <typeparam name="Vector2"></typeparam>
        /// <returns></returns>
        private  Drawing points = new Drawing();
        private  Drawing candidate = new Drawing();
        private  Drawing template = new Drawing();


        [OneTimeSetUp]
        public void SetupOnce()
        {
            gameManager= new GameObject();
            
            points.Add(Vector2.zero);
            points.Add(new Vector2(0, 1));
            points.Add(new Vector2(0, 2));
            points.Add(new Vector2(0, 3));

            candidate.Add(new Vector2(1,1));
            candidate.Add(new Vector2(1, 2));
            candidate.Add(new Vector2(2, 2));
            candidate.Add(new Vector2(2, 1));
            

            template.Add(new Vector2(1,1));
            template.Add(new Vector2(1.5f, 2));
            template.Add(new Vector2(2, 1));
            template.Add(new Vector2(1.5f,1));
        }

        /// <summary>
        /// This test checks that the PathLength function provides the correct
        /// length of the drawing
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator GiveGesturePathLength()
        {
            float length=Recognizer.PathLength(points);

            Assert.That(length, Is.EqualTo(3));
            yield return null;
        }

        /// <summary>
        /// This test checks that the list of dots corresponding to a drawing is
        /// resampled to the correct number of dots.
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator ResampleTheNumberOfPointsInTheList()
        {
            int finalNbofPoints = 2;

            Drawing newPoints= Recognizer.Resample(points, finalNbofPoints);

            Assert.That(newPoints.Count, Is.EqualTo(finalNbofPoints));
            yield return null;
        }

        /// <summary>
        /// This test checks that the vector coordinates corresponds to the
        /// centroid coordinates of the list of points provided
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator FindTheCentroidOfAListofVectors()
        {
            int indexLastPoint = points.Count - 1;
            Vector2 lineCentroid = new Vector2(points[indexLastPoint].x / 2,points[indexLastPoint].y/2);
            Vector2 centroid = Recognizer.Centroid(points);

            Assert.That(centroid.x, Is.EqualTo(lineCentroid.x));
            Assert.That(centroid.y, Is.EqualTo(lineCentroid.y));
            yield return null;
        }
        /// <summary>
        /// This test checks if the angle computed for the vector first point-centroid
        /// is correct
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator FindTheAngleBetween()
        {   
            float angleCentroid=Recognizer.IndicativeAngle(points);
            
            //the angle between the centroid of a line and vector zero
            float angleCentroidLine = Mathf.PI/2;

            Assert.That(angleCentroid, Is.EqualTo(angleCentroidLine));
            yield return null;
        }

        /// <summary>
        /// This test checks if all the points in the list are rotated by the
        /// angle provided
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator RotateThePointsInAList()
        {
            float angleCentroidLine = -Mathf.PI/2;
            int indexLastPoint = points.Count - 1;
            Vector2 lineCentroid = new Vector2(points[indexLastPoint].x / 2,points[indexLastPoint].y/2);
            Drawing rotatedPoints =Recognizer.RotateBy(points, angleCentroidLine);


            foreach( Vector2 point in rotatedPoints)
            {
                float differenceAngle= Mathf.Atan2(lineCentroid.y - point.y, lineCentroid.x - point.x);
                
                //To prevent gimbal lock issue
                if(differenceAngle<=Mathf.PI+0.001f)
                    differenceAngle = 0;

                Assert.That(differenceAngle, Is.EqualTo(0).Within(0.001));
            }
            yield return null;
        }
        /// <summary>
        /// This test checks if the points in the list are inside the define
        /// bounding box
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator DefineABoxAroundTheDrawing()
        {   
            Drawing Rectangle = Recognizer.BoundingBox(points);
            foreach(Vector2 point in points)
            {
                Assert.That(point.x,Is.GreaterThanOrEqualTo(Rectangle[0].x));
                Assert.That(point.y,Is.GreaterThanOrEqualTo(Rectangle[0].y));

                Assert.That(point.x,Is.LessThanOrEqualTo(Rectangle[1].x));
                Assert.That(point.y,Is.LessThanOrEqualTo(Rectangle[1].y));
            }

            yield return null;
        }
        /// <summary>
        /// This test checks if the size of the rescaling bounding box. However
        /// this test does not check for critical case like line or a point
        /// since ScaleTo is known for not handling these cases.
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator SizeOfTheScaledBoundingbox()
        {
            int size =250;
            Drawing newPoints = Recognizer.ScaleTo(points,size);
            Drawing rectangle = Recognizer.BoundingBox(newPoints);

            float rectangleHeigth = Vector2.Distance(new Vector2(rectangle[0].x, rectangle[1].y), rectangle[0]);
            float rectangleWidth= Vector2.Distance(new Vector2(rectangle[0].x, rectangle[1].y), rectangle[1]);

    
            // A tolerance of 0.05f is requested due to the offset introduced to
            // avoid dividing by 0.
            if (rectangleHeigth != 0)
            Assert.That(rectangleHeigth, Is.EqualTo(250).Within(0.5f));

            if (rectangleWidth != 0)
            Assert.That(rectangleWidth, Is.EqualTo(250).Within(0.5f));


            yield return null;
        }
        /// <summary>
        /// This test cheks if all the new centroid of the drawing correspond to
        /// the provided point corresponding to the origin
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator PointsAreTranslatedToANewPosition()
        {
            Vector2 origin = Vector2.zero;
            Drawing newPoints = Recognizer.TranslateTo(points, origin);

            Vector2 centroid = Recognizer.Centroid(newPoints);

            Assert.That(centroid.x,Is.EqualTo(origin.x));
            Assert.That(centroid.y,Is.EqualTo(origin.y));

            yield return null;
        }

        /// <summary>
        /// This test checks if the distance between the two list of points is correct.
        /// </summary>
        /// <returns></returns>
         [UnityTest]
        public IEnumerator DistanceBetweenTwoListsOfPoints()
        {
            float distanceToCandidate=Recognizer.PathDistance(candidate,candidate);
            float distanceToTemplate=Recognizer.PathDistance(candidate,template);

            Assert.That(distanceToCandidate,Is.EqualTo(0));
            Assert.That(distanceToTemplate,Is.EqualTo(0.5f));

            yield return null;
        }
        
        /// <summary>
        /// This test checks if the distance between the two list of points is
        /// correct within a tolerance of 0.5
        /// </summary>
        /// <returns></returns>
          [UnityTest]
        public IEnumerator FindTheDistanceAtBestAngleToComputeTheDistanceBetweenTheListOfPoints()
        {
            float angleLimitNegative = -Mathf.PI / 4;
            float angleLimitPositive = Mathf.PI / 4;
            float threshold = 2;

            float distanceToCandidate = Recognizer.DistanceAtBestAngle(candidate, candidate, angleLimitNegative, angleLimitPositive, threshold);
            float distanceToTemplate = Recognizer.DistanceAtBestAngle(candidate, template, angleLimitNegative, angleLimitPositive, threshold);
            
         
            Assert.That(distanceToCandidate,Is.EqualTo(0.13f).Within(0.01f));
            Assert.That(distanceToTemplate,Is.EqualTo(0.46f).Within(0.01f));

            yield return null;
        }
        /// <summary>
        /// This test checks if the scores computed,  the rating of a set
        /// of template for corresponding to candidate, is correct.
        /// </summary>
        /// <returns></returns>
        [UnityTest]
        public IEnumerator ComputeAScoreForASetOfTemplate()
        {
            List<Drawing> templates = new List<Drawing>();
            templates.Add(candidate);
            //Tolerance for the score (accuracy)
            Assert.That(Recognizer.Recognize(candidate,templates).Item2,Is.EqualTo(1).Within(0.1));
            
            List<Drawing> templatesBis = new List<Drawing>();
            templates.Add(template);
            Assert.That(Recognizer.Recognize(candidate,templates).Item2,Is.Not.EqualTo(1));
       
            
            yield return null;
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            Object.Destroy(gameManager); 
        }


    }

}
