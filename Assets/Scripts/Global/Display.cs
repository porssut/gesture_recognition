using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Provide the main function to interact and link with the UI with the main functionalities
/// </summary>
public class Display : MonoBehaviour
{   
    public InputField gestureName;
    public Text screenText;
    public Text scoreText;
    public Text executionTimeText;
    public Text drawingBoardText;
    public Text userManualText;
    public Text thresholdScoreText;
    public Slider thresholdScoreSlider;
    public BoardController drawingBoard;

    /// <summary>
    /// Flag to keep track if the current template has already been added
    /// </summary>
    /// <value></value>
    private bool buttonAlreadyClicked { get; set; }

    // Store last text 
    string lastScoreText;
    string lastThresholdScoreText;
    string lastExecutionTime;


    //===============//
    // Set Callbacks //
    //===============//

    Action AddTemplateButtonCallback;
    Action SaveButtonCallback;
    Action LoadButtonCallback;
    Action ExitButtonCallback;
    Action ValueChangedThresholdScoreCallback;

    /// <summary>
    /// Callback for OnClickAddTemplate()
    /// </summary>
    /// <param name="callback"></param>
    public void SetAddTemplateButtonCallback(Action callback) { AddTemplateButtonCallback = callback;}

    /// <summary>
    /// Callback for OnClickAddTemplate()
    /// </summary>
    /// <param name="callback"></param>
    public void SetSaveButtonCallback(Action callback) { SaveButtonCallback = callback;}
    /// <summary>
    /// Callback for OnClickLoad()
    /// </summary>
    /// <param name="callback"></param>
    public void SetLoadButtonCallback(Action callback) { LoadButtonCallback = callback;}
        /// <summary>
    /// Callback for OnClickExit()
    /// </summary>
    /// <param name="callback"></param>
    public void SetExitButtonCallback(Action callback) { ExitButtonCallback = callback;}
        /// <summary>
    /// Callback for OnValueChangedThresholdScore()
    /// </summary>
    /// <param name="callback"></param>
    public void SetValueChangedThresholdScoreSliderCallback(Action callback) { ValueChangedThresholdScoreCallback = callback;}


    //===================//
    // Trigger Callbacks //
    //===================//

    /// <summary>
    /// Activated by the button UI "Add template" and call AddCurrentTemplate 
    /// </summary>
    public void OnClickAddTemplate () {

        // Ensure both name field is set and that the drawing is not already added
        if ( gestureName.text == "" ) {
            screenText.text = "Please fill the field";
            return;
        }

        if ( buttonAlreadyClicked ) {
            screenText.text = "Drawing already added";
            return;
        }

        // Update button state
        buttonAlreadyClicked = true;

        // Trigger the callback
        if ( AddTemplateButtonCallback != null ) { AddTemplateButtonCallback(); }
    }

    /// <summary>
    /// Activated by the button UI "Save" 
    /// </summary>
    public void OnClickSave () {
        if ( SaveButtonCallback != null )
            SaveButtonCallback();
    }

    /// <summary>
    /// Activated by the button UI "Load" 
    /// </summary>
    public void OnClickLoad () {
        if ( LoadButtonCallback != null )
            LoadButtonCallback();
    }

    /// <summary>
    /// Quit the application.
    /// </summary>
    public void OnClickExit () {
        if ( ExitButtonCallback != null )
            ExitButtonCallback();
    }

    /// <summary>
    /// User can control the threshold below which the results from the
    /// recognizer is rejected
    /// </summary>
    /// <param name="threshold"></param>
    public void OnValueChangedThresholdScore ( Single valueNotUsed ) {
        if ( ValueChangedThresholdScoreCallback != null )
            ValueChangedThresholdScoreCallback();
    }


    /// <summary>
    /// Display information about the different buttons and parts
    /// </summary>
    public void OnClickManual () {
        userManualText.enabled = !userManualText.enabled;
        screenText.enabled = !screenText.enabled;
        if ( userManualText.IsActive() ) {
            lastScoreText = scoreText.text;
            lastThresholdScoreText = thresholdScoreText.text;
            lastExecutionTime = executionTimeText.text;

            scoreText.text = "Score";
            executionTimeText.text = "Execution Time";
            screenText.text = "Draw Here!";
            thresholdScoreText.text = "Threshold Score";
        } else {
            scoreText.text = lastScoreText;
            executionTimeText.text = lastExecutionTime;
            thresholdScoreText.text = lastThresholdScoreText;
            screenText.text = "";
        }
    }


	//=================//
	// Display methods //
	//=================//
	public void displayTemplateSaved () { screenText.text = "Templates saved"; }
    public void displaySavingFailed () { screenText.text = "Saving failed"; }
    public void displayNoTemplateSaved () { screenText.text = "No template added"; }
    public void displayNoXMLFileFound () { screenText.text = "No Xml files"; }
    public void displayNoXMLFolderFound () { screenText.text = "No Xml folder"; }
    public void displayTemplatesLoaded () { screenText.text = "Templates loaded"; }
    public void displayDrawingTooSmallWarning () { screenText.text = "Please draw more points"; }
    public void displayTemplateAdded () { screenText.text = "Drawing added"; }


    /// <summary>
    /// Remove information from the infos panel except the score threshold
    /// </summary>
    /// <param name="thresholdScorePercentage"></param>
    public void HideDisplayedInformation () {
        screenText.text = "";
        scoreText.text = "";
        gestureName.text = "";
        executionTimeText.text = "";
        drawingBoardText.text = "";
        buttonAlreadyClicked = false;
    }

    /// <summary>
    /// Display the name of the drawing, the execution time and the score on the panel infos.
    /// </summary>
    /// <param name="indexToTemplateName"></param>
    /// <param name="currentIndexScore"></param>
    /// <param name="executionTime"></param>
    /// <param name="thresholdScore"></param>
    public void DisplayGestureDrawnName ( Dictionary<int, string> indexToTemplateName, Tuple<int, float> currentIndexScore, float executionTime, float thresholdScore ) {
        if ( indexToTemplateName.ContainsKey( currentIndexScore.Item1 ) && currentIndexScore.Item2 > thresholdScore ) {
            gestureName.text = indexToTemplateName[currentIndexScore.Item1];
            screenText.text = indexToTemplateName[currentIndexScore.Item1];
            float scorePercentage = (float) Math.Round( currentIndexScore.Item2 * 100 );
            scoreText.text = scorePercentage.ToString() + "%";
        } else {
            screenText.text = "No matching template";
            float scorePercentage = (float) Math.Round( currentIndexScore.Item2 * 100 );
            scoreText.text = scorePercentage.ToString() + "%";
        }
        executionTimeText.text = executionTime.ToString() + " ms";
    }
    public void ShowPercentageThresholdScore () {
        float thresholdScorePercentage = (float) Math.Round( thresholdScoreSlider.value * 100 );
        thresholdScoreText.text = thresholdScorePercentage.ToString() + "%";
    }   
   
}