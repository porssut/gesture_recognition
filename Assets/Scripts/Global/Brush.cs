using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class allows to draw with the mouse.
/// </summary>
public class Brush: MonoBehaviour
{
    LineRenderer lineRenderer;

    Vector3 previousBrushPosition;

    /// <summary>
    /// the last drawing stored
    /// </summary>
    /// <value></value>
    public Drawing currentCandidate { get; private set; }


    void Start()
    {
        currentCandidate = new Drawing();
    }

    /// <summary>
    /// Intanciate and Initialise our brush
    /// </summary>
    public void CreateBrush() 
    {
        if(lineRenderer==null)
        lineRenderer = this.GetComponent<LineRenderer>();

        
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,Camera.main.nearClipPlane+0.01f));

        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, mousePos);
        lineRenderer.SetPosition(1, mousePos);
    }

    /// <summary>
    /// Sent an unique mouse position to the Draw function
    /// </summary>
    public void PointToMousePos() 
    {
        if (lineRenderer == null) { return; }
    
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,Camera.main.nearClipPlane+0.01f));
        
        if (previousBrushPosition == mousePos) { return; }

        Draw(mousePos);
        previousBrushPosition = mousePos;
    }
    /// <summary>
    /// This function fill the currentCandidate list with a drawing.
    /// </summary>
    public void StoreGestureDrawn()
    {
        Vector3[] candidate = new Vector3[lineRenderer.positionCount];

        lineRenderer.GetPositions(candidate);
        currentCandidate.Clear();
        currentCandidate = Drawing.From3DVectors(candidate);
    }

    /// <summary>
    /// Add a new vertex to the linerenderer component of our brush
    /// </summary>
    /// <param name="pointPos"></param>
    void Draw(Vector3 pointPos) 
    {   
            lineRenderer.positionCount++;
            int positionIndex = lineRenderer.positionCount - 1;
            lineRenderer.SetPosition(positionIndex, pointPos);
    }
    


}
