using System;
using System.Diagnostics;
using UnityEngine;

/// <summary>
/// This class allows to detect when the user draw on the surface (gameobject with a collider to which it is attached)
/// </summary>
public class BoardController : MonoBehaviour
{
    //===============//
    // Set Callbacks //
    //===============//

    Action OnMouseDownnDrawingBoardCallback;
    Action OnMouseUpDrawingBoardCallback;
    Action OnMouseOverDrawingBoardCallback;
    
    /// <summary>
    /// Callback for OnMouseDown()
    /// </summary>
    public void OnMouseDownDrawingBoard(Action callback) { OnMouseDownnDrawingBoardCallback = callback;}
    
    /// <summary>
    /// Callback for OnMouseDown()
    /// </summary>
    /// <param name="callback"></param>
    public void OnMouseUpDrawingBoard(Action callback) { OnMouseUpDrawingBoardCallback = callback;}
    
    /// <summary>
    /// Callback for OnMouseOver()
    /// </summary>
    /// <param name="callback"></param>
    public void OnMouseOverDrawingBoard(Action callback) { OnMouseOverDrawingBoardCallback = callback;}
 


    //===================//
    // Trigger Callbacks //
    //===================//


    void OnMouseDown()
    {   
        if(OnMouseDownnDrawingBoardCallback!=null)
            OnMouseDownnDrawingBoardCallback();
    }
    void OnMouseUp()
    { 
        if(OnMouseUpDrawingBoardCallback!=null)
            OnMouseUpDrawingBoardCallback();
        
    }
    void OnMouseOver()
    {
        if(OnMouseOverDrawingBoardCallback!=null)
            OnMouseOverDrawingBoardCallback();
         
    }


}
