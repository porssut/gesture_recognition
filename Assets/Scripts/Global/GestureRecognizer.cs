using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using Utilities;
using Algorithms;


/// <summary>
/// This class  handles the recognition of the drawing 
/// </summary>
public class GestureRecognizer
{

    /// <summary>
    ///  Set of template against which the drawing processed is compared
    /// </summary>
    /// <value></value>
    public List<List<Drawing>> preprocessedSetsOfTemplates { get; private set; }

    /// <summary>
    /// Store the  name of the template for the corresponding index in the preprocessedSetsOfTemplates
    /// </summary>
    /// <value></value>
    public Dictionary<int, string> indexToTemplateName { get; private set; }

    /// <summary>
    /// Store the index in the preprocessedSetsOfTemplates for the corresponding
    /// name of  template 
    /// </summary>
    /// <value></value>
    public Dictionary<string, int> templateNameToIndex { get; private set; }

    /// <summary>
    /// Last Preprocessed template
    /// </summary>
    Drawing currentPreprocessedTemplate;


    /// <summary>
    /// Returns a drawing resampled, rotated and translated to its centroid
    /// </summary>
    /// <param name="drawing"></param>
    /// <returns></returns>
    Drawing PreprocessListOfPoints(in Drawing drawing)
    {
        // These values are the one used in the original paper
        int timeStep = 64;
        int size = 250;

        Drawing preprocessedTemplates = Recognizer.Resample(drawing, timeStep);
        float indicativeAngle = Recognizer.IndicativeAngle(preprocessedTemplates);
        preprocessedTemplates = Recognizer.RotateBy(preprocessedTemplates, indicativeAngle);
        preprocessedTemplates = Recognizer.ScaleTo(preprocessedTemplates, size);
        return Recognizer.TranslateTo(preprocessedTemplates, Vector2.zero);
    }
    
    public GestureRecognizer()
    {
        preprocessedSetsOfTemplates = new List<List<Drawing>>();
        currentPreprocessedTemplate = new Drawing();
        indexToTemplateName = new Dictionary<int, string>();
        templateNameToIndex = new Dictionary<string, int>();

    }

    /// <summary>
    /// Returns the index and the score of the best set of templates ( the type
    /// of gesture)
    /// </summary>
    /// <param name="candidate"></param>
    /// <returns></returns>
    public Tuple<int, float> ComputeIndexScore(in Drawing candidate)
    {
        float scoreFinal = 0;
        int indexFinal = 0;

        currentPreprocessedTemplate = PreprocessListOfPoints(candidate);

        for (int i = 0; i < preprocessedSetsOfTemplates.Count; i++)
        {
            float score = Recognizer.Recognize(currentPreprocessedTemplate, preprocessedSetsOfTemplates[i]).Item2;

            if (score > scoreFinal)
            {
                scoreFinal = score;
                indexFinal = i;
            }
        }
        return new Tuple<int, float>(indexFinal, scoreFinal);
    }

    /// <summary>
    /// Add the new gesture drawn to the library of set of templates
    /// </summary>
    /// <param name="template"></param>
    /// <param name="templateName"></param>
    public void AddCurrentTemplate(string templateName)
    {
        AddLoadedTemplate(currentPreprocessedTemplate, templateName);
    }

     /// <summary>
    /// Add load gesture to the library of set of templates
    /// </summary>
    /// <param name="template"></param>
    /// <param name="templateName"></param>
    public void AddLoadedTemplate(Drawing preprocessedTemplateLoaded, string templateName)
    {
        if (indexToTemplateName.ContainsValue(templateName))
            preprocessedSetsOfTemplates[templateNameToIndex[templateName]].Add(preprocessedTemplateLoaded);
        else
        {
            indexToTemplateName.Add(preprocessedSetsOfTemplates.Count, templateName);
            templateNameToIndex.Add(templateName, preprocessedSetsOfTemplates.Count);
            List<Drawing> newSetOfTemplates = new List<Drawing>();
            newSetOfTemplates.Add(preprocessedTemplateLoaded);
            preprocessedSetsOfTemplates.Add(newSetOfTemplates);
        }
    }

    /// <summary>
    ///  Loop through all the templates to save them.
    /// </summary>
    /// <param name="folderPath"></param>
    /// <returns></returns>
    public void SaveTemplatesLists(string folderPath )
    {
        if (preprocessedSetsOfTemplates.Count == 0) { throw new Exception();}

        PrepareSavingFolder( folderPath );

        for (int i = 0; i < preprocessedSetsOfTemplates.Count; i++)
        {
            for (int j = 0; j < preprocessedSetsOfTemplates[i].Count; j++)
            {
                FileSystem.Save(folderPath, indexToTemplateName[i].ToString(), preprocessedSetsOfTemplates[i][j]);
            }
        }

    }

    /// <summary>
    /// Check if a directory exist and erase all the files to prevent duplicate
    /// </summary>
    /// <param name="folderPath"></param>
    private void PrepareSavingFolder( string folderPath )
    {
        // Create a Xml directory if it does not exist.
        Directory.CreateDirectory(folderPath);

        // Flush directory content
        try {
            // Delete previous template files
            foreach (string f in Directory.GetFiles(folderPath, "*.xml"))
                File.Delete(f);

        } catch ( Exception e) { Debug.LogError( e.ToString() ); }
    }

    /// <summary>
    ///   Loop through all the XML files to load them as new templates.
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public void LoadTemplatesLists(string filePath)
    {
        if (!Directory.Exists(filePath)) { throw new MissingFolderException(); }

        DirectoryInfo info = new DirectoryInfo(filePath);
        FileInfo[] fileInfo = info.GetFiles("*.xml");
        
        if (fileInfo.Length==0) { throw new MissingFileException();}

        foreach (FileInfo F in fileInfo)
        {
            AddLoadedTemplate(FileSystem.Load(F.FullName), Regex.Replace(F.Name, @"\d*.xml", ""));
        }
        
    }




}

