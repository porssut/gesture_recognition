using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class use to store the point of the drawing 
/// </summary>
public class Drawing : List<Vector2>
{
    /// <summary>
    /// Convert a 3D array to a 2D List
    /// </summary>
    /// <param name="ArrayToConvert"></param>
    /// <returns></returns>
    public static Drawing From3DVectors(Vector3[] ArrayToConvert)
    {
        Drawing ListConverted = new Drawing();

        foreach( Vector3 point in ArrayToConvert)
            ListConverted.Add(point);

        return ListConverted;
    }


}
