using System;
using System.Diagnostics;
using UnityEngine;
using Algorithms;

/// <summary>
/// This class is the main controller of the scene.
/// </summary>
public class SceneController : MonoBehaviour
{
    [Header("Binding")]
    public Brush brush;
    public Display display;
    public BoardController boardController;

    [Header("Settings")]
    public string filePath="./Xml";
    [Range(0,100)]
    public int minimalDrawingLength = 50;


    GestureRecognizer gestureRecognizer;
    float thresholdScore;
    Stopwatch stopwatch;
    Tuple<int, float> currentIndexScore;
    


    void Start()
    {
        //Attribution to the corresponding  callback
        boardController.OnMouseDownDrawingBoard(SetBoard);
        boardController.OnMouseOverDrawingBoard(DrawOnBoard);
        boardController.OnMouseUpDrawingBoard(RecognizeGestureDrawnName);

        display.SetAddTemplateButtonCallback(AddTemplate);
        display.SetSaveButtonCallback(SaveTemplates);
        display.SetLoadButtonCallback(LoadTemplate);
        display.SetValueChangedThresholdScoreSliderCallback(SetThresholdScore);
        display.SetExitButtonCallback(Application.Quit);

        stopwatch = new Stopwatch();
        gestureRecognizer = new GestureRecognizer();
        thresholdScore = 0.5f;

    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
    }

    /// <summary>
    /// Clean the board and prepare the brush to draw.
    /// </summary>
    void SetBoard()
    {   
        brush.CreateBrush();
        display.HideDisplayedInformation();
        display.ShowPercentageThresholdScore();
    }

    /// <summary>
    /// Draw on the board with the pen
    /// </summary>
    void DrawOnBoard() 
    {
        if(Input.GetMouseButton(0)) brush.PointToMousePos();
    }

    /// <summary>
    /// Call the method to generate an XML per drawing. 
    /// </summary>
    void SaveTemplates() {
		try {
            // Save templates
			gestureRecognizer.SaveTemplatesLists( filePath );

            UnityEngine.Debug.Log( "Templates saved" );
            display.displayTemplateSaved();

        } catch ( MissingFileException e ) {
            UnityEngine.Debug.LogWarning( e.ToString() );
			display.displaySavingFailed();

		} catch ( Exception e ) {
            UnityEngine.Debug.LogWarning( e.ToString() );
            display.displayNoTemplateSaved();

        }
    }

    /// <summary>
    /// Call the method to load each XML per drawing. 
    /// </summary>
    void LoadTemplate() {
        try {
            // Load templates
            gestureRecognizer.LoadTemplatesLists( filePath );

            UnityEngine.Debug.Log( "Templates loaded" );
            display.displayTemplatesLoaded();

        } catch ( MissingFolderException e ) {
            UnityEngine.Debug.LogWarning( e.ToString() );
            display.displayNoXMLFolderFound();

        } catch ( MissingFileException e ) {
            UnityEngine.Debug.LogWarning( e.ToString() );
            display.displayNoXMLFileFound();
		}
	}

	/// <summary>
	/// Update the threshold score based on the slider value set  by the user
	/// </summary>
	void SetThresholdScore () {
        // Store value
        thresholdScore = display.thresholdScoreSlider.value;

        // Update view
        display.ShowPercentageThresholdScore();
    }


	/// <summary>
	///  Identify and display the gesture name drawn by the user
	/// </summary>
	void RecognizeGestureDrawnName ()
    {
        brush.StoreGestureDrawn();

        // This condition prevents the critical case of the single point
        if ( !isDrawingValid( brush.currentCandidate ) ) return;

        //Measure  the execution time of the gesture recognizer
        stopwatch.Start();
        currentIndexScore=gestureRecognizer.ComputeIndexScore(brush.currentCandidate);
        stopwatch.Stop();

        display.DisplayGestureDrawnName(gestureRecognizer.indexToTemplateName, currentIndexScore,stopwatch.ElapsedMilliseconds,thresholdScore);
        stopwatch.Reset();
    }

    /// <summary>
    /// Call the method to a template.
    /// </summary>
    void AddTemplate()
    {
        // This condition prevents the critical case of the single point
        if ( !isDrawingValid( brush.currentCandidate ) ) return;

        // Store template
        gestureRecognizer.AddCurrentTemplate(display.gestureName.text );
        UnityEngine.Debug.Log( "Template added" );
        display.displayTemplateAdded();
    }

    /// <summary>
    /// Return threshold in percentage
    /// </summary>
    /// <returns></returns>
    float GetThresholdScorePercentage() {return (float)Math.Round(thresholdScore * 100);}


    /// <summary>
    /// Check if the drawing is valid
    /// </summary>
    /// <param name="drawing"></param>
    /// <returns></returns>
    public bool isDrawingValid ( Drawing drawing )
     {
        if ( drawing.Count >= minimalDrawingLength ) return true;

        UnityEngine.Debug.LogWarning( "Drawing too small" );
        display.displayDrawingTooSmallWarning();
        return false;
    }
 }