using System;
using System.Collections.Generic;
using UnityEngine;



namespace Algorithms
{
    /// <summary>
    /// Class with all the main functions use to recognize a gesture based on the
    /// pseudo code provided in the paper :
    /// Wobbrock, Jacob O., Andrew D. Wilson, and Yang Li. 
    /// "Gestures without libraries, toolkits or training: a $1 recognizer for user interface prototypes."
    /// In Proceedings of the 20th annual ACM symposium on User interface software and technology, pp. 159-168. 2007.
/// </summary>
    public static class Recognizer
    {
        /// <summary>
        /// Compute the total distance between each dot of the drawing
        /// </summary>
        /// <param name="drawing"></param>
        /// <returns></returns>
        public static float PathLength(in Drawing drawing)
        {
            float distance = 0;
            for (int i = 0; i < drawing.Count - 1; i++)
            {
                distance += Vector2.Distance(drawing[i], drawing[i + 1]);
            }
            return distance;
        }

        /// <summary>
        /// Resample the drawing to the number nbOfPointsAfterResampling
        /// </summary>
        /// <param name="drawing"></param>
        /// <param name="nbOfPointsAfterResampling"></param>
        /// <returns></returns>
        public static Drawing Resample(in Drawing drawing, int nbOfPointsAfterResampling)
        {
            float increment = PathLength(drawing) / (nbOfPointsAfterResampling - 1);
            float initialDistance = 0;
            Drawing newPoints = new Drawing();
            newPoints.Add(drawing[0]);

            for (int i = 1; i < drawing.Count; i++)
            {
                float distance = Vector2.Distance(drawing[i], drawing[i - 1]);
                if ((initialDistance + distance) >= increment)
                {
                    float newX = drawing[i - 1].x + ((increment - initialDistance) / distance) * (drawing[i].x - drawing[i - 1].x);
                    float newY = drawing[i - 1].y + ((increment - initialDistance) / distance) * (drawing[i].y - drawing[i - 1].y);
                    Vector2 newPoint = new Vector2(newX, newY);

                    newPoints.Add(newPoint);
                    drawing.Insert(i, newPoint);
                    initialDistance = 0;
                }
                else
                    initialDistance += distance;
            }
            return newPoints;
        }

        /// <summary>
        /// Find the centroid of the drawing
        /// </summary>
        /// <param name="drawing"></param>
        /// <returns></returns>
        public static Vector2 Centroid(in Drawing drawing)
        {
            float centroidX = 0;
            float centroidY = 0;

            foreach (Vector2 point in drawing)
            {
                centroidX += point.x;
                centroidY += point.y;
            }
            centroidX = centroidX / drawing.Count;
            centroidY = centroidY / drawing.Count;

            return new Vector2(centroidX, centroidY);
        }

        /// <summary>
        /// Compute the angle from the centroid of the drawing and the first
        /// point (resp. first point drawn by the user)
        /// </summary>
        /// <param name="drawing"></param>
        /// <returns></returns>
        public static float IndicativeAngle(in Drawing drawing)
        {
            Vector2 centroid = Centroid(drawing);

            return Mathf.Atan2(centroid.y - drawing[0].y, centroid.x - drawing[0].x);
        }

        /// <summary>
        /// Rotate the drawing by -angle to set this angle to 0 degree
        /// </summary>
        /// <param name="drawing"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Drawing RotateBy(in Drawing drawing, float angle)
        {
            Drawing newPoints = new Drawing();
            Vector2 centroid = Centroid(drawing);

            foreach (Vector2 point in drawing)
            {
                float newX = (point.x - centroid.x) * Mathf.Cos(angle) - (point.y - centroid.y) * Mathf.Sin(angle) + centroid.x;
                float newY = (point.x - centroid.x) * Mathf.Sin(angle) + (point.y - centroid.y) * Mathf.Cos(angle) + centroid.y;
                Vector2 newPoint = new Vector2(newX, newY);

                newPoints.Add(newPoint);
            }

            return newPoints;

        }
        /// <summary>
        ///  Returns a rectangle define by (xMin, yMin),(xMax,yMax) the
        ///  coordinates max and min in the drawing
        /// </summary>
        /// <param name="drawing"></param>
        /// <returns></returns>
        public static Drawing BoundingBox(in Drawing drawing)
        {
            float xMin = drawing[0].x;
            float yMin = drawing[0].y;
            float xMax = drawing[0].x;
            float yMax = drawing[0].y;

            foreach (Vector2 point in drawing)
            {
                if (point.x < xMin)
                    xMin = point.x;

                if (point.y < yMin)
                    yMin = point.y;

                if (point.x > xMax)
                    xMax = point.x;

                if (point.y > yMax)
                    yMax = point.y;
            }
            return new Drawing { new Vector3(xMin, yMin), new Vector3(xMax, yMax) }; ;
        }

        /// <summary>
        /// Scale the drawing so that the resulting bounding box will be of size^2 size.
        /// Size=250 is used for this application.
        /// </summary>
        /// <param name="drawing"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Drawing ScaleTo(in Drawing drawing, int size)
        {
            Drawing rectangle = BoundingBox(drawing);
            float rectangleHeigth = Vector2.Distance(new Vector2(rectangle[0].x, rectangle[1].y), rectangle[0]);
            float rectangleWidth = Vector2.Distance(new Vector2(rectangle[0].x, rectangle[1].y), rectangle[1]);

            Drawing newPoints = new Drawing();

            foreach (Vector2 point in drawing)
            {
                //Avoid dividing by zero and however it does not prevent
                // the scaling to fail if line or one dot
                if (rectangleHeigth == 0 || rectangleWidth == 0)
                {
                    float Offset = 0.0001f;

                    float newX = point.x * size / (rectangleWidth + 2 * Offset);
                    float newY = point.y * size / (rectangleHeigth + 2 * Offset);

                    newPoints.Add(new Vector2(newX, newY));
                }
                else
                {
                    float newX = point.x * size / rectangleWidth;
                    float newY = point.y * size / rectangleHeigth;

                    newPoints.Add(new Vector2(newX, newY));
                }
            }
            return newPoints;
        }

        /// <summary>
        /// Translate the drawing to a specific point ( origin)
        /// </summary>
        /// <param name="drawing"></param>
        /// <param name="origin"></param>
        /// <returns></returns>
        public static Drawing TranslateTo(in Drawing drawing, Vector2 origin)
        {
            Drawing newPoints = new Drawing();
            Vector2 centroid = Centroid(drawing);

            foreach (Vector2 point in drawing)
            {
                float newX = point.x + origin.x - centroid.x;
                float newY = point.y + origin.y - centroid.y;

                newPoints.Add(new Vector2(newX, newY));
            }
            return newPoints;
        }

        /// <summary>
        /// Return the distance between two drawings
        /// </summary>
        /// <param name="Candidate"></param>
        /// <param name="Template"></param>
        /// <returns></returns>
        public static float PathDistance(in Drawing candidate, in Drawing template)
        {
            float distance = 0;
            //Prevent the issue when one point is missing after resampling
            for (int i = 0; i < Mathf.Min(candidate.Count, template.Count); i++)
            {
                distance += Vector2.Distance(candidate[i], template[i]);
            }
            return distance / candidate.Count;
        }

        /// <summary>
        /// Return the distance between two drawings after
        /// rotating the candidate list
        /// </summary>
        /// <param name="Candidate"></param>
        /// <param name="Template"></param>
        /// <param name="angleLimit"></param>
        /// <returns></returns>
        public static float DistanceAtAngle(in Drawing Candidate, in Drawing Template, float angleLimit)
        {
            Drawing newPoints = RotateBy(Candidate, angleLimit);
            return PathDistance(newPoints, Template);
        }

        /// <summary>
        /// Return the distance between two drawings for the minimum
        /// angle between the template and candidate lists
        /// </summary>
        /// <param name="candidate"></param>
        /// <param name="template"></param>
        /// <param name="angleLimitNegative"></param>
        /// <param name="angleLimitPositive"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public static float DistanceAtBestAngle(in Drawing candidate, in Drawing template, float angleLimitNegative, float angleLimitPositive, float threshold)
        {
            float goldenRatio = 0.5f * (-1 + Mathf.Sqrt(5));
            float firstNewAngle = angleLimitNegative * goldenRatio + (1 - goldenRatio) * angleLimitPositive;
            float firstDistanceAtAngle = DistanceAtAngle(candidate, template, firstNewAngle);
            float secondNewAngle = angleLimitNegative * (1 - goldenRatio) + goldenRatio * angleLimitPositive;
            float secondDistanceAtAngle = DistanceAtAngle(candidate, template, secondNewAngle);

            while (Mathf.Abs(angleLimitPositive - angleLimitNegative) > threshold)
            {
                if (firstDistanceAtAngle < secondDistanceAtAngle)
                {
                    angleLimitPositive = secondNewAngle;
                    secondNewAngle = firstNewAngle;
                    secondDistanceAtAngle = firstDistanceAtAngle;
                    firstNewAngle = angleLimitNegative * goldenRatio + (1 - goldenRatio) * angleLimitPositive;
                    firstDistanceAtAngle = DistanceAtAngle(candidate, template, firstNewAngle);
                }
                else
                {
                    angleLimitNegative = firstNewAngle;
                    firstNewAngle = secondNewAngle;
                    firstDistanceAtAngle = secondDistanceAtAngle;
                    secondNewAngle = angleLimitNegative * (1 - goldenRatio) + goldenRatio * angleLimitPositive;
                    secondDistanceAtAngle = DistanceAtAngle(candidate, template, secondNewAngle);
                }
            }
            return Mathf.Min(firstDistanceAtAngle, secondDistanceAtAngle);
        }

        /// <summary>
        /// Return a score (the rating of a set of template for corresponding to candidate) for the set of template 
        /// </summary>
        /// <param name="Candidate"></param>
        /// <param name="Templates"></param>
        /// <returns></returns>
        public static Tuple<Drawing, float> Recognize(in Drawing Candidate, in List<Drawing> Templates)
        {
            float newDistanceAtBestAngle = Mathf.Infinity;
            float angleLimitNegative = -Mathf.PI / 4;
            float angleLimitPositive = Mathf.PI / 4;
            float threshold = 2 * Mathf.Deg2Rad;
            float size = 250;
            Drawing bestTemplate = new Drawing();
            float score = 0;

            foreach (Drawing template in Templates)
            {
                float distanceAtBestAngle = DistanceAtBestAngle(Candidate, template, angleLimitNegative, angleLimitPositive, threshold);
                if (distanceAtBestAngle < newDistanceAtBestAngle)
                {
                    newDistanceAtBestAngle = distanceAtBestAngle;
                    bestTemplate = template;
                }
            }
            score = 1 - (float)newDistanceAtBestAngle / (float)(0.5 * Mathf.Sqrt(2 * Mathf.Pow(size, 2)));
            return new Tuple<Drawing, float>(bestTemplate, score);
        }




    }
}
