using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Text.RegularExpressions;

namespace Utilities
{
    /// <summary>
    /// This class handles the de/serialization of list of vector 2D.
    /// </summary>
    static class FileSystem 
    {

        /// <summary>
        /// Save a file storing list of 2D vectors in XML format:
        /// xml version="1.0"
        ///xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <param name="currentCandidate"></param>
        /// <returns></returns>
        public static void Save(string filePath,string fileName, Drawing currentCandidate)
        {
            int i = 0;

            // Prevent multiple templates override file with same name from overwriting
            fileName = filePath + "/"+fileName+i.ToString();
            while (File.Exists(fileName + ".xml"))
            {
                i++;
                fileName = Regex.Replace(fileName, @"\d", "");
                fileName += i.ToString();
            }

            string path = fileName+".xml";
            var serializer = new XmlSerializer(typeof(Drawing));

            using (var stream = new FileStream(path, FileMode.Create))
            serializer.Serialize(stream, currentCandidate);

            if (!File.Exists(path)) { throw new MissingFileException(); }

        }

        /// <summary>
        /// Load a file storing list of 2D vectors in XML format:
        /// xml version="1.0"
        /// xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Drawing Load(string path)
        {
            var serializer = new XmlSerializer(typeof(Drawing));

            using (var stream = new FileStream(path, FileMode.Open))
            return serializer.Deserialize(stream) as Drawing;

        }
    }

}

