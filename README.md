# Gesture Recognizer

[![pipeline status](https://gitlab.com/porssut/gesture_recognition/badges/main/pipeline.svg)](https://gitlab.com/porssut/gesture_recognition/-/commits/main)

![Gesture Recognizer](/Documentation/resources/ApplicationMainPanel.PNG)

This Unity project is an implementation of the [**$1 gesture recognizer**](https://depts.washington.edu/acelab/proj/dollar/index.html) based on
[pseudocode](https://depts.washington.edu/acelab/proj/dollar/dollar.pdf) provided by the paper authors:  
[Wobbrock, Jacob O., Andrew D. Wilson, and Yang Li. "Gestures without libraries, toolkits or training: a $1 recognizer for user interface prototypes." In Proceedings of the 20th annual ACM symposium on User interface software and technology, pp. 159-168. 2007.](https://depts.washington.edu/acelab/proj/dollar/index.html)


## Description

![Exemple](/Documentation/resources/ApplicationArrow.PNG)*This image illustrates an
drawing from the user as an Arrow at 90% with only one template trained*

Users can train (Add button) the recognizer with their own gesture (drawn on the
left panel) and save/load them for a next session. 

## Notice
The following instructions have to be followed:
* To save the gestures, they have to be added first. 
* The gestures are saved in a Xml folder next to executable. The folder will be
  created if no Xml folder.
* To load the gestures, they need to be inside the Xml folder.

## Requirement
The application currently works only on Windows 64bits plateform.

## Play The Application
* First download the build from the [release page](https://gitlab.com/porssut/gesture_recognition/-/releases).
* Launch the build and Enjoy! 

## Building
The application does not support resizing window. Indeed only the canvas will
resize but not the gameobjects. Thus, the option **Resizable Window** needs to be
uncheked. The resolution need to be fixed and standard (4k HUD,QHD,FullHD,WXGA).

## Code Documentation
The documentation of the code can be found [here](https://porssut.gitlab.io/gesture_recognition/)


>  
> *- Thibault Porssut*
