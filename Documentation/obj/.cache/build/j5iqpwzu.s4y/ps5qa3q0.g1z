<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Innovation Design Challenge: Gesture Recognizer | Unity Project: Gesture Recognition </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Innovation Design Challenge: Gesture Recognizer | Unity Project: Gesture Recognition ">
    <meta name="generator" content="docfx 2.58.0.0">
    
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="styles/docfx.vendor.css">
    <link rel="stylesheet" href="styles/docfx.css">
    <link rel="stylesheet" href="styles/main.css">
    <meta property="docfx:navrel" content="toc.html">
    <meta property="docfx:tocrel" content="toc.html">
    
    <meta property="docfx:rel" content="">
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="index.html">
                <img id="logo" class="svg" src="logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div class="container body-content">
        
        <div id="search-results">
          <div class="search-list">Search Results for <span></span></div>
          <div class="sr-items">
            <p><i class="glyphicon glyphicon-refresh index-loading"></i></p>
          </div>
          <ul id="pagination" data-first="First" data-prev="Previous" data-next="Next" data-last="Last"></ul>
        </div>
      </div>
      <div role="main" class="container body-content hide-when-search">
        <div class="article row grid">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="">
<h1 id="innovation-design-challenge-gesture-recognizer" sourcefile="index.md" sourcestartlinenumber="1" sourceendlinenumber="1">Innovation Design Challenge: Gesture Recognizer</h1>

<p sourcefile="index.md" sourcestartlinenumber="3" sourceendlinenumber="3"><a href="https://gitlab.com/porssut/gesture_recognition/-/commits/main" data-raw-source="[![pipeline status](https://gitlab.com/porssut/gesture_recognition/badges/main/pipeline.svg)](https://gitlab.com/porssut/gesture_recognition/-/commits/main)" sourcefile="index.md" sourcestartlinenumber="3" sourceendlinenumber="3"><img src="https://gitlab.com/porssut/gesture_recognition/badges/main/pipeline.svg" alt="pipeline status" sourcefile="index.md" sourcestartlinenumber="3" sourceendlinenumber="3"></a></p>
<p sourcefile="index.md" sourcestartlinenumber="5" sourceendlinenumber="5"><img src="/Documentation/images/ApplicationMainPanel.PNG" alt="Gesture Recognizer" sourcefile="index.md" sourcestartlinenumber="5" sourceendlinenumber="5"></p>
<p sourcefile="index.md" sourcestartlinenumber="7" sourceendlinenumber="9">This Unity project is an implementation of the <a href="https://depts.washington.edu/acelab/proj/dollar/index.html" data-raw-source="[**$1 gesture recognizer**](https://depts.washington.edu/acelab/proj/dollar/index.html)" sourcefile="index.md" sourcestartlinenumber="7" sourceendlinenumber="7"><strong>$1 gesture recognizer</strong></a> based on
<a href="https://depts.washington.edu/acelab/proj/dollar/dollar.pdf" data-raw-source="[pseudocode](https://depts.washington.edu/acelab/proj/dollar/dollar.pdf)" sourcefile="index.md" sourcestartlinenumber="8" sourceendlinenumber="8">pseudocode</a> provided by the paper authors:<br><a href="https://depts.washington.edu/acelab/proj/dollar/index.html" data-raw-source="[Wobbrock, Jacob O., Andrew D. Wilson, and Yang Li. &quot;Gestures without libraries, toolkits or training: a $1 recognizer for user interface prototypes.&quot; In Proceedings of the 20th annual ACM symposium on User interface software and technology, pp. 159-168. 2007.](https://depts.washington.edu/acelab/proj/dollar/index.html)" sourcefile="index.md" sourcestartlinenumber="9" sourceendlinenumber="9">Wobbrock, Jacob O., Andrew D. Wilson, and Yang Li. &quot;Gestures without libraries, toolkits or training: a $1 recognizer for user interface prototypes.&quot; In Proceedings of the 20th annual ACM symposium on User interface software and technology, pp. 159-168. 2007.</a></p>
<h2 id="description" sourcefile="index.md" sourcestartlinenumber="12" sourceendlinenumber="12">Description</h2>
<p sourcefile="index.md" sourcestartlinenumber="14" sourceendlinenumber="15"><img src="/Documentation/images/ApplicationArrow.PNG" alt="Exemple" sourcefile="index.md" sourcestartlinenumber="14" sourceendlinenumber="14"><em>This image illustrates an
drawing from the user as an Arrow at 90% with only one template trained</em></p>
<p sourcefile="index.md" sourcestartlinenumber="17" sourceendlinenumber="18">Users can train (Add button) the recognizer with their own gesture (drawn on the
left panel) and save/load them for a next session. </p>
<h2 id="notice" sourcefile="index.md" sourcestartlinenumber="20" sourceendlinenumber="20">Notice</h2>
<p sourcefile="index.md" sourcestartlinenumber="21" sourceendlinenumber="21">The following instructions have to be followed:</p>
<ul sourcefile="index.md" sourcestartlinenumber="22" sourceendlinenumber="25">
<li sourcefile="index.md" sourcestartlinenumber="22" sourceendlinenumber="22">To save the gestures, they have to be added first. </li>
<li sourcefile="index.md" sourcestartlinenumber="23" sourceendlinenumber="24">The gestures are saved in a Xml folder next to executable. The folder will be
created if no Xml folder.</li>
<li sourcefile="index.md" sourcestartlinenumber="25" sourceendlinenumber="25">To load the gestures, they need to be inside the Xml folder.</li>
</ul>
<h2 id="requirement" sourcefile="index.md" sourcestartlinenumber="27" sourceendlinenumber="27">Requirement</h2>
<p sourcefile="index.md" sourcestartlinenumber="28" sourceendlinenumber="28">The application currently works only on Windows 64bits plateform.</p>
<h2 id="play-the-application" sourcefile="index.md" sourcestartlinenumber="30" sourceendlinenumber="30">Play The Application</h2>
<ul sourcefile="index.md" sourcestartlinenumber="31" sourceendlinenumber="32">
<li sourcefile="index.md" sourcestartlinenumber="31" sourceendlinenumber="31">First download the build from the <a href="" data-raw-source="[release page]()" sourcefile="index.md" sourcestartlinenumber="31" sourceendlinenumber="31">release page</a>.</li>
<li sourcefile="index.md" sourcestartlinenumber="32" sourceendlinenumber="32">Launch the build and Enjoy! </li>
</ul>
<h2 id="code-documentation" sourcefile="index.md" sourcestartlinenumber="34" sourceendlinenumber="34">Code Documentation</h2>
<p sourcefile="index.md" sourcestartlinenumber="35" sourceendlinenumber="35">The documentation of the code can be found <a href="" data-raw-source="[here]()" sourcefile="index.md" sourcestartlinenumber="35" sourceendlinenumber="35">here</a></p>
<h1 id="" sourcefile="index.md" sourcestartlinenumber="38" sourceendlinenumber="38">#</h1>
<blockquote sourcefile="index.md" sourcestartlinenumber="40" sourceendlinenumber="41"><p sourcefile="index.md" sourcestartlinenumber="40" sourceendlinenumber="41"><strong>I&#39;m looking forward to discuss with you!</strong><br><em>- Thibault Porssut</em></p>
</blockquote>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            Unity Project: Gesture Recognition
            
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="styles/docfx.js"></script>
    <script type="text/javascript" src="styles/main.js"></script>
  </body>
</html>
