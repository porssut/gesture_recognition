<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Class Recognizer
   | Unity Project: Gesture Recognition </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Class Recognizer
   | Unity Project: Gesture Recognition ">
    <meta name="generator" content="docfx 2.58.0.0">
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../styles/docfx.css">
    <link rel="stylesheet" href="../styles/main.css">
    <meta property="docfx:navrel" content="../toc.html">
    <meta property="docfx:tocrel" content="toc.html">
    
    <meta property="docfx:rel" content="../">
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../index.html">
                <img id="logo" class="svg" src="../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div class="container body-content">
        
        <div id="search-results">
          <div class="search-list">Search Results for <span></span></div>
          <div class="sr-items">
            <p><i class="glyphicon glyphicon-refresh index-loading"></i></p>
          </div>
          <ul id="pagination" data-first="First" data-prev="Previous" data-next="Next" data-last="Last"></ul>
        </div>
      </div>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="Algorithms.Recognizer">
  
  
  <h1 id="Algorithms_Recognizer" data-uid="Algorithms.Recognizer" class="text-break">Class Recognizer
  </h1>
  <div class="markdown level0 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="6">Class with all the main functions use to recognize a gesture based on the
pseudo code provided in the paper :
Wobbrock, Jacob O., Andrew D. Wilson, and Yang Li. 
&quot;Gestures without libraries, toolkits or training: a $1 recognizer for user interface prototypes.&quot;
In Proceedings of the 20th annual ACM symposium on User interface software and technology, pp. 159-168. 2007.</p>
</div>
  <div class="markdown level0 conceptual"></div>
  <div class="inheritance">
    <h5>Inheritance</h5>
    <div class="level0"><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.object">Object</a></div>
    <div class="level1"><span class="xref">Recognizer</span></div>
  </div>
  <h6><strong>Namespace</strong>: <a class="xref" href="Algorithms.html">Algorithms</a></h6>
  <h6><strong>Assembly</strong>: cs.temp.dll.dll</h6>
  <h5 id="Algorithms_Recognizer_syntax">Syntax</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static class Recognizer</code></pre>
  </div>
  <h3 id="methods">Methods
  </h3>
  
  
  <a id="Algorithms_Recognizer_BoundingBox_" data-uid="Algorithms.Recognizer.BoundingBox*"></a>
  <h4 id="Algorithms_Recognizer_BoundingBox_Drawing__" data-uid="Algorithms.Recognizer.BoundingBox(Drawing@)">BoundingBox(in Drawing)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="3">Returns a rectangle define by (xMin, yMin),(xMax,yMax) the
coordinates max and min in the drawing</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static Drawing BoundingBox(in Drawing drawing)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_Centroid_" data-uid="Algorithms.Recognizer.Centroid*"></a>
  <h4 id="Algorithms_Recognizer_Centroid_Drawing__" data-uid="Algorithms.Recognizer.Centroid(Drawing@)">Centroid(in Drawing)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Find the centroid of the drawing</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static Vector2 Centroid(in Drawing drawing)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><span class="xref">Vector2</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_DistanceAtAngle_" data-uid="Algorithms.Recognizer.DistanceAtAngle*"></a>
  <h4 id="Algorithms_Recognizer_DistanceAtAngle_Drawing__Drawing__System_Single_" data-uid="Algorithms.Recognizer.DistanceAtAngle(Drawing@,Drawing@,System.Single)">DistanceAtAngle(in Drawing, in Drawing, Single)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="3">Return the distance between two drawings after
rotating the candidate list</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static float DistanceAtAngle(in Drawing Candidate, in Drawing Template, float angleLimit)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">Candidate</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">Template</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td><span class="parametername">angleLimit</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_DistanceAtBestAngle_" data-uid="Algorithms.Recognizer.DistanceAtBestAngle*"></a>
  <h4 id="Algorithms_Recognizer_DistanceAtBestAngle_Drawing__Drawing__System_Single_System_Single_System_Single_" data-uid="Algorithms.Recognizer.DistanceAtBestAngle(Drawing@,Drawing@,System.Single,System.Single,System.Single)">DistanceAtBestAngle(in Drawing, in Drawing, Single, Single, Single)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="3">Return the distance between two drawings for the minimum
angle between the template and candidate lists</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static float DistanceAtBestAngle(in Drawing candidate, in Drawing template, float angleLimitNegative, float angleLimitPositive, float threshold)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">candidate</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">template</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td><span class="parametername">angleLimitNegative</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td><span class="parametername">angleLimitPositive</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td><span class="parametername">threshold</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_IndicativeAngle_" data-uid="Algorithms.Recognizer.IndicativeAngle*"></a>
  <h4 id="Algorithms_Recognizer_IndicativeAngle_Drawing__" data-uid="Algorithms.Recognizer.IndicativeAngle(Drawing@)">IndicativeAngle(in Drawing)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="3">Compute the angle from the centroid of the drawing and the first
point (resp. first point drawn by the user)</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static float IndicativeAngle(in Drawing drawing)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_PathDistance_" data-uid="Algorithms.Recognizer.PathDistance*"></a>
  <h4 id="Algorithms_Recognizer_PathDistance_Drawing__Drawing__" data-uid="Algorithms.Recognizer.PathDistance(Drawing@,Drawing@)">PathDistance(in Drawing, in Drawing)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Return the distance between two drawings</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static float PathDistance(in Drawing candidate, in Drawing template)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">candidate</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">template</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_PathLength_" data-uid="Algorithms.Recognizer.PathLength*"></a>
  <h4 id="Algorithms_Recognizer_PathLength_Drawing__" data-uid="Algorithms.Recognizer.PathLength(Drawing@)">PathLength(in Drawing)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Compute the total distance between each dot of the drawing</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static float PathLength(in Drawing drawing)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_Recognize_" data-uid="Algorithms.Recognizer.Recognize*"></a>
  <h4 id="Algorithms_Recognizer_Recognize_Drawing__List_Drawing___" data-uid="Algorithms.Recognizer.Recognize(Drawing@,List{Drawing}@)">Recognize(in Drawing, in List&lt;Drawing&gt;)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Return a score (the rating of a set of template for corresponding to candidate) for the set of template </p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static Tuple&lt;Drawing, float&gt; Recognize(in Drawing Candidate, in List&lt;Drawing&gt; Templates)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">Candidate</span></td>
        <td></td>
      </tr>
      <tr>
        <td><span class="xref">List</span>&lt;<a class="xref" href="Global.Drawing.html">Drawing</a>&gt;</td>
        <td><span class="parametername">Templates</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.tuple-2">Tuple</a>&lt;<a class="xref" href="Global.Drawing.html">Drawing</a>, <a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a>&gt;</td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_Resample_" data-uid="Algorithms.Recognizer.Resample*"></a>
  <h4 id="Algorithms_Recognizer_Resample_Drawing__System_Int32_" data-uid="Algorithms.Recognizer.Resample(Drawing@,System.Int32)">Resample(in Drawing, Int32)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Resample the drawing to the number nbOfPointsAfterResampling</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static Drawing Resample(in Drawing drawing, int nbOfPointsAfterResampling)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.int32">Int32</a></td>
        <td><span class="parametername">nbOfPointsAfterResampling</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_RotateBy_" data-uid="Algorithms.Recognizer.RotateBy*"></a>
  <h4 id="Algorithms_Recognizer_RotateBy_Drawing__System_Single_" data-uid="Algorithms.Recognizer.RotateBy(Drawing@,System.Single)">RotateBy(in Drawing, Single)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Rotate the drawing by -angle to set this angle to 0 degree</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static Drawing RotateBy(in Drawing drawing, float angle)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a></td>
        <td><span class="parametername">angle</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_ScaleTo_" data-uid="Algorithms.Recognizer.ScaleTo*"></a>
  <h4 id="Algorithms_Recognizer_ScaleTo_Drawing__System_Int32_" data-uid="Algorithms.Recognizer.ScaleTo(Drawing@,System.Int32)">ScaleTo(in Drawing, Int32)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="3">Scale the drawing so that the resulting bounding box will be of size^2 size.
Size=250 is used for this application.</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static Drawing ScaleTo(in Drawing drawing, int size)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.int32">Int32</a></td>
        <td><span class="parametername">size</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Algorithms_Recognizer_TranslateTo_" data-uid="Algorithms.Recognizer.TranslateTo*"></a>
  <h4 id="Algorithms_Recognizer_TranslateTo_Drawing__Vector2_" data-uid="Algorithms.Recognizer.TranslateTo(Drawing@,Vector2)">TranslateTo(in Drawing, Vector2)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Algorithms.Recognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Translate the drawing to a specific point ( origin)</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public static Drawing TranslateTo(in Drawing drawing, Vector2 origin)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">drawing</span></td>
        <td></td>
      </tr>
      <tr>
        <td><span class="xref">Vector2</span></td>
        <td><span class="parametername">origin</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            Unity Project: Gesture Recognition
            
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../styles/docfx.js"></script>
    <script type="text/javascript" src="../styles/main.js"></script>
  </body>
</html>
