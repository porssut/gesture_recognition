<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Namespace Global
   | Unity Project: Gesture Recognition </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Namespace Global
   | Unity Project: Gesture Recognition ">
    <meta name="generator" content="docfx 2.58.0.0">
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../styles/docfx.css">
    <link rel="stylesheet" href="../styles/main.css">
    <meta property="docfx:navrel" content="../toc.html">
    <meta property="docfx:tocrel" content="toc.html">
    
    <meta property="docfx:rel" content="../">
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../index.html">
                <img id="logo" class="svg" src="../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div class="container body-content">
        
        <div id="search-results">
          <div class="search-list">Search Results for <span></span></div>
          <div class="sr-items">
            <p><i class="glyphicon glyphicon-refresh index-loading"></i></p>
          </div>
          <ul id="pagination" data-first="First" data-prev="Previous" data-next="Next" data-last="Last"></ul>
        </div>
      </div>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="Global">
  
  <h1 id="Global" data-uid="Global" class="text-break">Namespace Global
  </h1>
  <div class="markdown level0 summary"><p sourcefile="../Assets/Scripts/Global/Global.md" sourcestartlinenumber="1" sourceendlinenumber="1">More general classes.</p>
</div>
  <div class="markdown level0 conceptual"></div>
  <div class="markdown level0 remarks"></div>
    <h3 id="classes">Classes
  </h3>
      <h4><a class="xref" href="Global.BoardController.html">BoardController</a></h4>
      <section><p sourcefile="classes/Global.BoardController.yml" sourcestartlinenumber="2" sourceendlinenumber="2">This class allows to detect when the user draw on the surface (gameobject with a collider to which it is attached)</p>
</section>
      <h4><a class="xref" href="Global.Brush.html">Brush</a></h4>
      <section><p sourcefile="classes/Global.Brush.yml" sourcestartlinenumber="2" sourceendlinenumber="2">This class allows to draw with the mouse.</p>
</section>
      <h4><a class="xref" href="Global.Display.html">Display</a></h4>
      <section><p sourcefile="classes/Global.Display.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Provide the main function to interact and link with the UI with the main functionalities</p>
</section>
      <h4><a class="xref" href="Global.Drawing.html">Drawing</a></h4>
      <section><p sourcefile="classes/Global.Drawing.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Class use to store the point of the drawing </p>
</section>
      <h4><a class="xref" href="Global.GestureRecognizer.html">GestureRecognizer</a></h4>
      <section><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">This class  handles the recognition of the drawing </p>
</section>
      <h4><a class="xref" href="Global.ListTooSmallException.html">ListTooSmallException</a></h4>
      <section></section>
      <h4><a class="xref" href="Global.MissingFileException.html">MissingFileException</a></h4>
      <section></section>
      <h4><a class="xref" href="Global.MissingFolderException.html">MissingFolderException</a></h4>
      <section></section>
      <h4><a class="xref" href="Global.SceneController.html">SceneController</a></h4>
      <section><p sourcefile="classes/Global.SceneController.yml" sourcestartlinenumber="2" sourceendlinenumber="2">This class is the main controller of the scene.</p>
</section>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            Unity Project: Gesture Recognition
            
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../styles/docfx.js"></script>
    <script type="text/javascript" src="../styles/main.js"></script>
  </body>
</html>
