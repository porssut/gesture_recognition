<!DOCTYPE html>
<!--[if IE]><![endif]-->
<html>
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Class GestureRecognizer
   | Unity Project: Gesture Recognition </title>
    <meta name="viewport" content="width=device-width">
    <meta name="title" content="Class GestureRecognizer
   | Unity Project: Gesture Recognition ">
    <meta name="generator" content="docfx 2.58.0.0">
    
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../styles/docfx.vendor.css">
    <link rel="stylesheet" href="../styles/docfx.css">
    <link rel="stylesheet" href="../styles/main.css">
    <meta property="docfx:navrel" content="../toc.html">
    <meta property="docfx:tocrel" content="toc.html">
    
    <meta property="docfx:rel" content="../">
    
  </head>
  <body data-spy="scroll" data-target="#affix" data-offset="120">
    <div id="wrapper">
      <header>
        
        <nav id="autocollapse" class="navbar navbar-inverse ng-scope" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="../index.html">
                <img id="logo" class="svg" src="../logo.svg" alt="">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
              <form class="navbar-form navbar-right" role="search" id="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="search-query" placeholder="Search" autocomplete="off">
                </div>
              </form>
            </div>
          </div>
        </nav>
        
        <div class="subnav navbar navbar-default">
          <div class="container hide-when-search" id="breadcrumb">
            <ul class="breadcrumb">
              <li></li>
            </ul>
          </div>
        </div>
      </header>
      <div class="container body-content">
        
        <div id="search-results">
          <div class="search-list">Search Results for <span></span></div>
          <div class="sr-items">
            <p><i class="glyphicon glyphicon-refresh index-loading"></i></p>
          </div>
          <ul id="pagination" data-first="First" data-prev="Previous" data-next="Next" data-last="Last"></ul>
        </div>
      </div>
      <div role="main" class="container body-content hide-when-search">
        
        <div class="sidenav hide-when-search">
          <a class="btn toc-toggle collapse" data-toggle="collapse" href="#sidetoggle" aria-expanded="false" aria-controls="sidetoggle">Show / Hide Table of Contents</a>
          <div class="sidetoggle collapse" id="sidetoggle">
            <div id="sidetoc"></div>
          </div>
        </div>
        <div class="article row grid-right">
          <div class="col-md-10">
            <article class="content wrap" id="_content" data-uid="Global.GestureRecognizer">
  
  
  <h1 id="Global_GestureRecognizer" data-uid="Global.GestureRecognizer" class="text-break">Class GestureRecognizer
  </h1>
  <div class="markdown level0 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">This class  handles the recognition of the drawing </p>
</div>
  <div class="markdown level0 conceptual"></div>
  <div class="inheritance">
    <h5>Inheritance</h5>
    <div class="level0"><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.object">Object</a></div>
    <div class="level1"><span class="xref">GestureRecognizer</span></div>
  </div>
  <h6><strong>Namespace</strong>: <a class="xref" href="Global.html">Global</a></h6>
  <h6><strong>Assembly</strong>: cs.temp.dll.dll</h6>
  <h5 id="Global_GestureRecognizer_syntax">Syntax</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public class GestureRecognizer</code></pre>
  </div>
  <h3 id="constructors">Constructors
  </h3>
  
  
  <a id="Global_GestureRecognizer__ctor_" data-uid="Global.GestureRecognizer.#ctor*"></a>
  <h4 id="Global_GestureRecognizer__ctor" data-uid="Global.GestureRecognizer.#ctor">GestureRecognizer()</h4>
  <div class="markdown level1 summary"></div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public GestureRecognizer()</code></pre>
  </div>
  <h3 id="properties">Properties
  </h3>
  
  
  <a id="Global_GestureRecognizer_indexToTemplateName_" data-uid="Global.GestureRecognizer.indexToTemplateName*"></a>
  <h4 id="Global_GestureRecognizer_indexToTemplateName" data-uid="Global.GestureRecognizer.indexToTemplateName">indexToTemplateName</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Store the  name of the template for the corresponding index in the preprocessedSetsOfTemplates</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public Dictionary&lt;int, string&gt; indexToTemplateName { get; }</code></pre>
  </div>
  <h5 class="propertyValue">Property Value</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><span class="xref">Dictionary</span>&lt;<a class="xref" href="https://docs.microsoft.com/dotnet/api/system.int32">Int32</a>, <a class="xref" href="https://docs.microsoft.com/dotnet/api/system.string">String</a>&gt;</td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Global_GestureRecognizer_preprocessedSetsOfTemplates_" data-uid="Global.GestureRecognizer.preprocessedSetsOfTemplates*"></a>
  <h4 id="Global_GestureRecognizer_preprocessedSetsOfTemplates" data-uid="Global.GestureRecognizer.preprocessedSetsOfTemplates">preprocessedSetsOfTemplates</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Set of template against which the drawing processed is compared</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public List&lt;List&lt;Drawing&gt;&gt; preprocessedSetsOfTemplates { get; }</code></pre>
  </div>
  <h5 class="propertyValue">Property Value</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><span class="xref">List</span>&lt;<span class="xref">List</span>&lt;<a class="xref" href="Global.Drawing.html">Drawing</a>&gt;&gt;</td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Global_GestureRecognizer_templateNameToIndex_" data-uid="Global.GestureRecognizer.templateNameToIndex*"></a>
  <h4 id="Global_GestureRecognizer_templateNameToIndex" data-uid="Global.GestureRecognizer.templateNameToIndex">templateNameToIndex</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="3">Store the index in the preprocessedSetsOfTemplates for the corresponding
name of  template </p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public Dictionary&lt;string, int&gt; templateNameToIndex { get; }</code></pre>
  </div>
  <h5 class="propertyValue">Property Value</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><span class="xref">Dictionary</span>&lt;<a class="xref" href="https://docs.microsoft.com/dotnet/api/system.string">String</a>, <a class="xref" href="https://docs.microsoft.com/dotnet/api/system.int32">Int32</a>&gt;</td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h3 id="methods">Methods
  </h3>
  
  
  <a id="Global_GestureRecognizer_AddCurrentTemplate_" data-uid="Global.GestureRecognizer.AddCurrentTemplate*"></a>
  <h4 id="Global_GestureRecognizer_AddCurrentTemplate_System_String_" data-uid="Global.GestureRecognizer.AddCurrentTemplate(System.String)">AddCurrentTemplate(String)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Add the new gesture drawn to the library of set of templates</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public void AddCurrentTemplate(string templateName)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.string">String</a></td>
        <td><span class="parametername">templateName</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Global_GestureRecognizer_AddLoadedTemplate_" data-uid="Global.GestureRecognizer.AddLoadedTemplate*"></a>
  <h4 id="Global_GestureRecognizer_AddLoadedTemplate_Drawing_System_String_" data-uid="Global.GestureRecognizer.AddLoadedTemplate(Drawing,System.String)">AddLoadedTemplate(Drawing, String)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Add load gesture to the library of set of templates</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public void AddLoadedTemplate(Drawing preprocessedTemplateLoaded, string templateName)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">preprocessedTemplateLoaded</span></td>
        <td></td>
      </tr>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.string">String</a></td>
        <td><span class="parametername">templateName</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Global_GestureRecognizer_ComputeIndexScore_" data-uid="Global.GestureRecognizer.ComputeIndexScore*"></a>
  <h4 id="Global_GestureRecognizer_ComputeIndexScore_Drawing__" data-uid="Global.GestureRecognizer.ComputeIndexScore(Drawing@)">ComputeIndexScore(in Drawing)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="3">Returns the index and the score of the best set of templates ( the type
of gesture)</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public Tuple&lt;int, float&gt; ComputeIndexScore(in Drawing candidate)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="Global.Drawing.html">Drawing</a></td>
        <td><span class="parametername">candidate</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <h5 class="returns">Returns</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.tuple-2">Tuple</a>&lt;<a class="xref" href="https://docs.microsoft.com/dotnet/api/system.int32">Int32</a>, <a class="xref" href="https://docs.microsoft.com/dotnet/api/system.single">Single</a>&gt;</td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Global_GestureRecognizer_LoadTemplatesLists_" data-uid="Global.GestureRecognizer.LoadTemplatesLists*"></a>
  <h4 id="Global_GestureRecognizer_LoadTemplatesLists_System_String_" data-uid="Global.GestureRecognizer.LoadTemplatesLists(System.String)">LoadTemplatesLists(String)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Loop through all the XML files to load them as new templates.</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public void LoadTemplatesLists(string filePath)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.string">String</a></td>
        <td><span class="parametername">filePath</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  
  
  <a id="Global_GestureRecognizer_SaveTemplatesLists_" data-uid="Global.GestureRecognizer.SaveTemplatesLists*"></a>
  <h4 id="Global_GestureRecognizer_SaveTemplatesLists_System_String_" data-uid="Global.GestureRecognizer.SaveTemplatesLists(System.String)">SaveTemplatesLists(String)</h4>
  <div class="markdown level1 summary"><p sourcefile="classes/Global.GestureRecognizer.yml" sourcestartlinenumber="2" sourceendlinenumber="2">Loop through all the templates to save them.</p>
</div>
  <div class="markdown level1 conceptual"></div>
  <h5 class="decalaration">Declaration</h5>
  <div class="codewrapper">
    <pre><code class="lang-csharp hljs">public void SaveTemplatesLists(string folderPath)</code></pre>
  </div>
  <h5 class="parameters">Parameters</h5>
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Type</th>
        <th>Name</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a class="xref" href="https://docs.microsoft.com/dotnet/api/system.string">String</a></td>
        <td><span class="parametername">folderPath</span></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</article>
          </div>
          
          <div class="hidden-sm col-md-2" role="complementary">
            <div class="sideaffix">
              <div class="contribution">
                <ul class="nav">
                </ul>
              </div>
              <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix" id="affix">
                <h5>In This Article</h5>
                <div></div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <footer>
        <div class="grad-bottom"></div>
        <div class="footer">
          <div class="container">
            <span class="pull-right">
              <a href="#top">Back to top</a>
            </span>
            Unity Project: Gesture Recognition
            
          </div>
        </div>
      </footer>
    </div>
    
    <script type="text/javascript" src="../styles/docfx.vendor.js"></script>
    <script type="text/javascript" src="../styles/docfx.js"></script>
    <script type="text/javascript" src="../styles/main.js"></script>
  </body>
</html>
